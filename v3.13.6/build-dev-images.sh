ln -s config.dev/config-pg-es7.properties config.properties
docker build -t nubisware/conductor-server:3.13.6-dev -f Dockerfile-server .
docker push nubisware/conductor-server:3.13.6-dev
unlink config.properties

# Override fetch plugin with one that uses d4s-boot secure fetch
#cp config/fetch.js conductor/ui/src/plugins/fetch.js

# Override root App with one instantiating d4s-boot configured for dev
#cp config.dev/App.jsx conductor/ui/src/App.jsx

# jump to ui code and build
#cd conductor/ui/
#yarn install && yarn build
#cd -

# copy the built app to local folder and build Docker image. The clean up.
#cp -r conductor/ui/build .
#ln -s config.dev/nginx/conf.d/default.conf default.conf
#docker build -t nubisware/conductor-frontend:3.13.6-dev -f Dockerfile-frontend .
#rm -rf build
#unlink default.conf

#docker push nubisware/conductor-frontend:dev
