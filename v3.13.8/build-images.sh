#!/usr/bin/env bash
if [ $# -eq 0 ]
  then
    tag='dev'
  else
    tag=$1
fi

CONDUCTOR_VERSION=v3.13.8

rm -rf conductor conductor-community
git clone https://github.com/Netflix/conductor -b $CONDUCTOR_VERSION
git clone https://github.com/Netflix/conductor-community -b $CONDUCTOR_VERSION
find conductor-community/ -name dependencies.lock -exec rm -v {} \;

# Build conductor server
ln -s config.$tag/config.properties config.properties
ln -s config/startup.sh startup.sh
docker build --no-cache -t hub.dev.d4science.org/conductor/server:$CONDUCTOR_VERSION-$tag -f Dockerfile-server .
docker push hub.dev.d4science.org/conductor/server:$CONDUCTOR_VERSION-$tag
unlink config.properties
unlink startup.sh

exit 0

# Override fetch plugin with one that uses d4s-boot secure fetch
cp config/fetch.js conductor/ui/src/plugins/fetch.js

# Override root App with one instantiating d4s-boot configured for dev
cp config.$tag/App.jsx conductor/ui/src/App.jsx

# jump to ui code and build
cd conductor/ui/
yarn install && yarn build
cd -

# copy the built app to local folder and build Docker image. The clean up.
cp -r conductor/ui/build .
ln -s config.$tag/nginx/conf.d/default.conf default.conf
docker build --no-cache -t hub.dev.d4science.org/conductor/frontend:$CONDUCTOR_VERSION-$tag -f Dockerfile-frontend .
#docker push hub.dev.d4science.org/conductor/frontend:$CONDUCTOR_VERSION-$tag
rm -rf build
unlink default.conf
