This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "conductor-setup"

## [v0.2.0]
- Factored out workflows
- Added relational persistence
- Removed Dynomite
- Added workers

## [v0.1.0-SNAPSHOT]

- First release. It provides Conductor HA with 2 instances. (#19689).<br>
  Currently the Docker Swamrs Stack deploys:
    - 4 Dynomites nodes (2 shards with 1 replication each one, handled by Dynomite directly) backed by Redis DB in the same container
    - 2 Conductor server nodes with 2 replicas handled by Swarm
    - 2 Conductor UI nodes with 2 replicas handled by Swarm
    - 1 Elasticsearch node

