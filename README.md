# Conductor Setup

**Conductor Setup** is composed of a set of ansible roles and a playbook named site-*.yaml useful for deploying a docker swarm running Conductor microservice orchestrator by [Netflix OSS](https://netflix.github.io/conductor/). 

Current setup is based on Conductor 3.0.4 version adapted by Nubisware S.r.l.
It uses the docker images on dockerhub:

- nubisware/conductor-server:3.0.4
- nubisware/conductor-ui-oauth2:3.0.4 (which has been improved with Oauth2 login in collaboration with Keycloak)

Besides the basic components Conductor itself (server and ui) and Elasticsearch 6.1.8, the repository can be configured to launch postgres or mysql persistence plus basic python based workers for running PyRest, PyMail, PyExec and PyShell in the same Swarm.
In addition a nginx based PEP can be executed to protect the conductor REST API server.
It is also possible to connect to an external postgres for stateful deployments.

## Structure of the project

The folder roles contains the necessary roles for configuring the different configurations
There are 4 file for deploying to local, nw-cluster or D4SCience dev, pre and prod environments.

To run a deployment

`ansible-playbook site-X.yaml`

whereas
 
`ansible-playbook site-X.yaml -e dry=true`

only generates the files for the stack without actually deploying it.

The folder *local-site* contains a ready version for quickly launching a conductor instance with no replications (except workers), no auth in the Conductor UI and no PEP.

`docker stack deploy -c elasticsearch-swarm.yaml -c postgres-swarm.yaml -c conductor-swarm.yaml -c conductor-workers-swarm.yaml -c pep-swarm.yaml conductor`

When you have ensured that Postgres and Elasticsearch are running, execute:

`docker stack deploy -c conductor-swarm.yaml conductor`

This will create a local stack accessible through permissive pep at port 80. Please add two mappings for localhost in your /etc/hosts

`127.0.1.1	conductor-server conductor-ui`

and point your browser to http://conductor-ui.

## Built With

* [Ansible](https://www.ansible.com)
* [Docker](https://www.docker.com)


## Examples

Checkout the files site-X.yaml as a reference for different configurations.

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Marco Lettere** ([Nubisware S.r.l.](http://www.nubisware.com))
* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]
