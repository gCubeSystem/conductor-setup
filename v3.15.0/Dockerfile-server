#

# ===========================================================================================================
# 0. Builder stage
# ===========================================================================================================
FROM eclipse-temurin:17-jdk-jammy AS builder

LABEL maintainer="Nubisware SRL"

RUN apt-get update && apt-get install -y git

# Copy the project directly onto the image
COPY ./conductor-community /conductor
WORKDIR /conductor

# Build the server on run
#RUN ./gradlew generateLock updateLock saveLock
RUN ./gradlew build -x test --stacktrace

# ===========================================================================================================
# 1. Bin stage
# ===========================================================================================================
FROM eclipse-temurin:17-jdk-jammy

LABEL maintainer="Nubisware SRL"

# Make app folders
RUN mkdir -p /app/config /app/logs /app/libs

# Copy the compiled output to new image
COPY --from=builder /conductor/community-server/build/libs/conductor-community-server-*-SNAPSHOT-boot.jar /app/libs/conductor-server.jar
COPY ./config.properties /app/config.properties
COPY ./startup.sh /app/startup.sh

HEALTHCHECK --interval=60s --timeout=30s --retries=10 CMD curl -I -XGET http://localhost:8080/health || exit 1

CMD [ "/app/startup.sh" ]
ENTRYPOINT [ "/bin/sh"]

